import { v4 as uuidv4 } from 'uuid'

enum CurrencyEnum {
  USD = 'USD',
  UAH = 'UAH',
}

class Transaction {
  id: string
  amount: number
  currency: CurrencyEnum

  constructor (amount: number, currency: CurrencyEnum) {
    this.id = uuidv4()
    this.amount = amount
    this.currency = currency
  }
}

class Card {
  transactions: Transaction[] = []

  addTransaction (transaction: Transaction): string
  addTransaction (amount: number, currency: CurrencyEnum): string
  addTransaction (transactionOrAmount: Transaction | number, currency?: CurrencyEnum): string {
    if (typeof transactionOrAmount === 'number' && (currency != null)) {
      const newTransaction = new Transaction(transactionOrAmount, currency)
      this.transactions.push(newTransaction)
      return newTransaction.id
    } else {
      const typedTransaction = transactionOrAmount as Transaction
      this.transactions.push(typedTransaction)
      return typedTransaction.id
    }
  }

  getTransaction (id: string): Transaction | undefined {
    return this.transactions.find((transaction) => transaction.id === id)
  }

  getBalance (currency: CurrencyEnum): number {
    return this.transactions
      .filter((transaction) => transaction.currency === currency)
      .reduce((total, transaction) => total + transaction.amount, 0)
  }
}

const card = new Card()

const transactionId1 = card.addTransaction(new Transaction(100, CurrencyEnum.USD))
console.log(card.getTransaction(transactionId1))

const transactionId2 = card.addTransaction(200, CurrencyEnum.UAH)
console.log(card.getTransaction(transactionId2))

console.log(card.getBalance(CurrencyEnum.USD))
console.log(card.getBalance(CurrencyEnum.UAH))
