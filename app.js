"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("uuid");
var CurrencyEnum;
(function (CurrencyEnum) {
    CurrencyEnum["USD"] = "USD";
    CurrencyEnum["UAH"] = "UAH";
})(CurrencyEnum || (CurrencyEnum = {}));
var Transaction = /** @class */ (function () {
    function Transaction(amount, currency) {
        this.id = (0, uuid_1.v4)();
        this.amount = amount;
        this.currency = currency;
    }
    return Transaction;
}());
var Card = /** @class */ (function () {
    function Card() {
        this.transactions = [];
    }
    Card.prototype.addTransaction = function (transactionOrAmount, currency) {
        if (typeof transactionOrAmount === 'number' && (currency != null)) {
            var newTransaction = new Transaction(transactionOrAmount, currency);
            this.transactions.push(newTransaction);
            return newTransaction.id;
        }
        else {
            var typedTransaction = transactionOrAmount;
            this.transactions.push(typedTransaction);
            return typedTransaction.id;
        }
    };
    Card.prototype.getTransaction = function (id) {
        return this.transactions.find(function (transaction) { return transaction.id === id; });
    };
    Card.prototype.getBalance = function (currency) {
        return this.transactions
            .filter(function (transaction) { return transaction.currency === currency; })
            .reduce(function (total, transaction) { return total + transaction.amount; }, 0);
    };
    return Card;
}());
var card = new Card();
var transactionId1 = card.addTransaction(new Transaction(100, CurrencyEnum.USD));
console.log(card.getTransaction(transactionId1));
var transactionId2 = card.addTransaction(200, CurrencyEnum.UAH);
console.log(card.getTransaction(transactionId2));
console.log(card.getBalance(CurrencyEnum.USD));
console.log(card.getBalance(CurrencyEnum.UAH));
